# Installation
 - `git clone https://gitlab.com/Dunarr/validateur-de-validateur-de-formulaire.git validateur`
 - `cd validateur`
 - `npm install`
 - Mettre votre code dans le dossier src

# Lancer les tests
 - `npm run test`

# Mise à jour
 - `git pull`