/* This Cucumber.js tutorial file contains the step definition or the description of each of the behavior that is expected from the application */

'use strict';

const {Given, When, Then} = require('@cucumber/cucumber');
const {By, Key, until} = require('selenium-webdriver');

const assert = require('assert')

const webdriver = require('selenium-webdriver');

// // The step definitions are defined for each of the scenarios // //

// // The “given” condition for our test scenario // //
Given(/^Je visite la page ([^"]*)$/, function (url, next) {

    this.driver.get("http://localhost:4444"+url).then(next);

});
Given(/^Je remplie le champs "([^"]*)" avec "([^"]*)"$/, async function (field, value){
    /** @var WebDriver this.driver */
    const fieldEl = await this.driver.findElement(By.name(field))
    await fieldEl.sendKeys(value||"")
})
When("J'envoie le formulaire", async function (){
    const submitBtn = await this.driver.findElement(By.css('input[type="submit"]'))
    await submitBtn.click()
})
Then(/^Je devrait avoir l'erreur "([^"]*)"$/, async function (message) {
    const messageEl = await this.driver.findElement(By.className('error'))
    const content = await messageEl.getText()
    assert.ok(!!content.match(new RegExp(message)), `Wrong error message: expected "${content}" doesn't includes "${message}"`)
});
Then('Je devrait avoir un message de succès', async function () {

    const messageEl = await this.driver.findElement(By.className('success'))
    const content = await messageEl.getText()
    assert.equal(content, "compte créé", `Wrong error message: expecting  "compte créé", got "${content}"`)
});
Then('Je ne devrait pas avoir d\'erreur', async function () {

    const errorMessages = await this.driver.findElements(By.className('error'))
    assert.equal(errorMessages.length, 0, `The form is well completed but there is an error message`)
});