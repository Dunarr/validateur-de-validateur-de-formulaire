const express = require('express')
const app = express();
const path = require("path")

app.use(express.static(path.join(__dirname, "..","..","src")))


var server = null

module.exports = {
    start: function (port = 4444){
        server = app.listen(port)
    },
    stop: function (){
        server.close()
    }
}