// // This Cucumber.js tutorial support file to perform validations and initialization for our app // //

const server = require('./server')
const {setWorldConstructor, setDefaultTimeout, After, Before} = require('@cucumber/cucumber')

const selenium = require('selenium-webdriver');
var firefox = require('selenium-webdriver/firefox');

var chrome = require('selenium-webdriver/chrome');


function CustomWorld() {

    this.driver = new selenium.Builder()
        .forBrowser('firefox')
        .build();
}
selenium

setWorldConstructor(CustomWorld)
setDefaultTimeout(30*1000)
Before(function() {
    server.start()
});
After(function() {
    server.stop()
    return this.driver.quit();
});
module.exports = function () {

    this.World = CustomWorld;

    this.setDefaultTimeout(30 * 1000);
};