# language: fr
Fonctionnalité: Validation de formulaire en javascript

Scénario: : Verification du username obligatoire
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "username" avec ""
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "password" avec "superpassword$01"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Identifiant obligatoire"

Scénario: : Verification de la longueur du username
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "username" avec "a"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "password" avec "superpassword$01"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Identifiant trop court"

Scénario: : Verification de la longueur du mot de passe
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "password" avec "b"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Mot de passe trop court trop court"

Scénario: : Verification des caractères spéciaux du mot de passe
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "password" avec "monsupermdp"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Le mot de passe doit contenir au moin un caractère spécial"

Scénario: : Verification des chiffres du mot de passe
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "password" avec "monsupermdp@"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Le mot de passe doit contenir au moins un chiffre"

Scénario: : Verification de la confirmation de mot de passe
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "password" avec "monsupermdp%123"
  Et que Je remplie le champs "confirm-password" avec "monsupermdp%12"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Les mots de passe ne correspondent pas"

Scénario: : Verification du mail
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "mail" avec "william.com@test"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "password" avec "superpassword$01"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Le mail est invalide"

Scénario: : Verification du téléphone 1
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "tel" avec "06010101"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "password" avec "superpassword$01"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Le téléphone est invalide"

Scénario: : Verification du téléphone 2
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "tel" avec "ab06010101"
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "password" avec "superpassword$01"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir l'erreur "Le téléphone est invalide"

Scénario: : Verification de la validation du formulaire
  Etant donné que Je visite la page /index.html
  Et que Je remplie le champs "mail" avec "william@dunarr.com"
  Et que Je remplie le champs "username" avec "xXdunarrXxdu72"
  Et que Je remplie le champs "password" avec "superpassword$01"
  Et que Je remplie le champs "confirm-password" avec "superpassword$01"
  Et que Je remplie le champs "tel" avec "0601020304"
  Lorsque J'envoie le formulaire
  Alors Je devrait avoir un message de succès
  Et Je ne devrait pas avoir d'erreur
